﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pos_print.clase
{
    class Empresa
    {
        public string empresa_ruc { get; set; }
        public string empresa_razon_social { get; set; }
        public string empresa_direccion { get; set; }
        public string empresa_telefono { get; set; }
        public string empresa_correo { get; set; }
        public string empresa_estado { get; set; }
        public string empresa_icono { get; set; }
        public string empresa_abreviatura { get; set; }
        public string empresa_nombre_comercial { get; set; }
        public object empresa_usuario_sol { get; set; }
        public object empresa_clave_sol { get; set; }
        public object empreasa_firma_digital { get; set; }
        public string empresa_estado_activo { get; set; }
        public string empresa_token_facturacion { get; set; }
        public string empresa_fondo { get; set; }
        public string empresa_color { get; set; }
        public string empresa_link_facturacion { get; set; }
    }
}
