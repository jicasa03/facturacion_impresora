﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pos_print.clase
{
    class LoginResponse
    {
        public bool ok { get; set; }
        public string msg { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string empleado_dni { get; set; }
        public string empleado_id { get; set; }
        public string empleado_perfil { get; set; }
        public string usuario { get; set; }
        public string perfil_descripcion { get; set; }
        public string ruc { get; set; }


    }
}
