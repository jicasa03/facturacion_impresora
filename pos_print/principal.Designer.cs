﻿namespace pos_print
{
    partial class principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(principal));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.titulo_nombre = new System.Windows.Forms.Label();
            this.titulo_perfil = new System.Windows.Forms.Label();
            this.image_logo = new System.Windows.Forms.PictureBox();
            this.titulo_empresa = new System.Windows.Forms.Label();
            this.nombre_impresora = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.image_logo)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(84, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 69);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(223, 115);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 69);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(347, 115);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 69);
            this.button3.TabIndex = 2;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(449, 232);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(44, 42);
            this.button4.TabIndex = 12;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // titulo_nombre
            // 
            this.titulo_nombre.AutoSize = true;
            this.titulo_nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titulo_nombre.Location = new System.Drawing.Point(43, 213);
            this.titulo_nombre.Name = "titulo_nombre";
            this.titulo_nombre.Size = new System.Drawing.Size(72, 13);
            this.titulo_nombre.TabIndex = 13;
            this.titulo_nombre.Text = "cargando...";
            // 
            // titulo_perfil
            // 
            this.titulo_perfil.AutoSize = true;
            this.titulo_perfil.Location = new System.Drawing.Point(46, 232);
            this.titulo_perfil.Name = "titulo_perfil";
            this.titulo_perfil.Size = new System.Drawing.Size(61, 13);
            this.titulo_perfil.TabIndex = 14;
            this.titulo_perfil.Text = "cargando...";
            // 
            // image_logo
            // 
            this.image_logo.Location = new System.Drawing.Point(12, 12);
            this.image_logo.Name = "image_logo";
            this.image_logo.Size = new System.Drawing.Size(112, 59);
            this.image_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.image_logo.TabIndex = 15;
            this.image_logo.TabStop = false;
            // 
            // titulo_empresa
            // 
            this.titulo_empresa.AutoSize = true;
            this.titulo_empresa.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titulo_empresa.Location = new System.Drawing.Point(139, 35);
            this.titulo_empresa.Name = "titulo_empresa";
            this.titulo_empresa.Size = new System.Drawing.Size(95, 19);
            this.titulo_empresa.TabIndex = 16;
            this.titulo_empresa.Text = "Cargando...";
            this.titulo_empresa.Click += new System.EventHandler(this.label1_Click);
            // 
            // nombre_impresora
            // 
            this.nombre_impresora.Location = new System.Drawing.Point(347, 187);
            this.nombre_impresora.Name = "nombre_impresora";
            this.nombre_impresora.Size = new System.Drawing.Size(140, 39);
            this.nombre_impresora.TabIndex = 17;
            this.nombre_impresora.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 286);
            this.Controls.Add(this.nombre_impresora);
            this.Controls.Add(this.titulo_empresa);
            this.Controls.Add(this.image_logo);
            this.Controls.Add(this.titulo_perfil);
            this.Controls.Add(this.titulo_nombre);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "principal";
            this.Load += new System.EventHandler(this.principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.image_logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label titulo_nombre;
        private System.Windows.Forms.Label titulo_perfil;
        private System.Windows.Forms.PictureBox image_logo;
        private System.Windows.Forms.Label titulo_empresa;
        private System.Windows.Forms.Label nombre_impresora;
    }
}