﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.Http;
using Newtonsoft.Json;
using System.Configuration;
namespace pos_print.conexion
{

    class Conexion
    {
        //String url_global = "http://172.16.1.14/webservice_seguro/";

        public async Task<string> Envio_seguro(string url, String json)
        {

            string url_web = ConfigurationManager.AppSettings["url_web"];

            /*  var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(new
              {
                  Username = "myusername",
                  Password = "pass"
              }));
              */
            // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            String responseContent = "";
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZF9kaXNwb3NpdHZvIjoiOWZhZTQ3MGJkNDljODUxNSJ9.ZAnApn6H7-K7Op9ajyo99icrxJX3TcR6aforan3y3KY");
                // Do the actual request and await the response
                var httpResponse = await httpClient.PostAsync(url_web + url, httpContent);

                // If the response contains content we want to read it!
                if (httpResponse.Content != null)
                {
                    responseContent = await httpResponse.Content.ReadAsStringAsync();

                    //  Debug.WriteLine(responseContent);

                    //     Debug.WriteLine(responseContent);
                    // From here on you could deserialize the ResponseContent back again to a concrete C# type using Json.Net
                }
            }
            return responseContent;
        }
    }


}
