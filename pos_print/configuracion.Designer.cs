﻿namespace pos_print
{
    partial class configuracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.input_url_web = new System.Windows.Forms.TextBox();
            this.input_node_url = new System.Windows.Forms.TextBox();
            this.input_nombre_server = new System.Windows.Forms.TextBox();
            this.input_puerto = new System.Windows.Forms.TextBox();
            this.input_base_datos = new System.Windows.Forms.TextBox();
            this.Guardar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.input_usuario_base = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.input_clave_base = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.input_serie_ticket = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.input_codigo_auto = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(289, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "CONFIGURACION  DE SERVIDOR";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // input_url_web
            // 
            this.input_url_web.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.input_url_web.Location = new System.Drawing.Point(37, 66);
            this.input_url_web.Name = "input_url_web";
            this.input_url_web.Size = new System.Drawing.Size(288, 20);
            this.input_url_web.TabIndex = 1;
            this.input_url_web.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // input_node_url
            // 
            this.input_node_url.Location = new System.Drawing.Point(37, 111);
            this.input_node_url.Name = "input_node_url";
            this.input_node_url.Size = new System.Drawing.Size(288, 20);
            this.input_node_url.TabIndex = 2;
            // 
            // input_nombre_server
            // 
            this.input_nombre_server.Location = new System.Drawing.Point(37, 158);
            this.input_nombre_server.Name = "input_nombre_server";
            this.input_nombre_server.Size = new System.Drawing.Size(288, 20);
            this.input_nombre_server.TabIndex = 3;
            // 
            // input_puerto
            // 
            this.input_puerto.Location = new System.Drawing.Point(37, 200);
            this.input_puerto.Name = "input_puerto";
            this.input_puerto.Size = new System.Drawing.Size(288, 20);
            this.input_puerto.TabIndex = 4;
            // 
            // input_base_datos
            // 
            this.input_base_datos.Location = new System.Drawing.Point(37, 243);
            this.input_base_datos.Name = "input_base_datos";
            this.input_base_datos.Size = new System.Drawing.Size(288, 20);
            this.input_base_datos.TabIndex = 5;
            // 
            // Guardar
            // 
            this.Guardar.BackColor = System.Drawing.Color.RoyalBlue;
            this.Guardar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guardar.ForeColor = System.Drawing.SystemColors.Control;
            this.Guardar.Location = new System.Drawing.Point(63, 492);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(96, 31);
            this.Guardar.TabIndex = 6;
            this.Guardar.Text = "Guardar";
            this.Guardar.UseVisualStyleBackColor = false;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.Control;
            this.button2.Location = new System.Drawing.Point(209, 492);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 31);
            this.button2.TabIndex = 7;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "URL WEB";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "URL NODE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(34, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "NOMBRE DE SERVIDOR";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(34, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "PUERTO DE CONEXIÓN";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(34, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "N. BASE DE DATOS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(34, 275);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "USUARIO BASE";
            // 
            // input_usuario_base
            // 
            this.input_usuario_base.Location = new System.Drawing.Point(37, 293);
            this.input_usuario_base.Name = "input_usuario_base";
            this.input_usuario_base.Size = new System.Drawing.Size(288, 20);
            this.input_usuario_base.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(34, 324);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "CLAVE BASE";
            // 
            // input_clave_base
            // 
            this.input_clave_base.Location = new System.Drawing.Point(37, 342);
            this.input_clave_base.Name = "input_clave_base";
            this.input_clave_base.Size = new System.Drawing.Size(288, 20);
            this.input_clave_base.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(34, 376);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 15);
            this.label9.TabIndex = 18;
            this.label9.Text = "SERIE TICKETERA";
            // 
            // input_serie_ticket
            // 
            this.input_serie_ticket.Location = new System.Drawing.Point(37, 394);
            this.input_serie_ticket.Name = "input_serie_ticket";
            this.input_serie_ticket.Size = new System.Drawing.Size(288, 20);
            this.input_serie_ticket.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(34, 427);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 15);
            this.label10.TabIndex = 20;
            this.label10.Text = "CODIGO AUTORIZACION";
            // 
            // input_codigo_auto
            // 
            this.input_codigo_auto.Location = new System.Drawing.Point(37, 445);
            this.input_codigo_auto.Name = "input_codigo_auto";
            this.input_codigo_auto.Size = new System.Drawing.Size(288, 20);
            this.input_codigo_auto.TabIndex = 19;
            // 
            // configuracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 535);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.input_codigo_auto);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.input_serie_ticket);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.input_clave_base);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.input_usuario_base);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.input_base_datos);
            this.Controls.Add(this.input_puerto);
            this.Controls.Add(this.input_nombre_server);
            this.Controls.Add(this.input_node_url);
            this.Controls.Add(this.input_url_web);
            this.Controls.Add(this.label1);
            this.Name = "configuracion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.configuracion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox input_url_web;
        private System.Windows.Forms.TextBox input_node_url;
        private System.Windows.Forms.TextBox input_nombre_server;
        private System.Windows.Forms.TextBox input_puerto;
        private System.Windows.Forms.TextBox input_base_datos;
        private System.Windows.Forms.Button Guardar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox input_usuario_base;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox input_clave_base;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox input_serie_ticket;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox input_codigo_auto;
    }
}