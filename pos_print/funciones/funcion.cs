﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using System.Xml;
namespace pos_print.funciones
{
    class funcion
    {


        public void cargardatoslogin(String nombre, String apellido, String id, String perfil, String usuario)
        {
            Debug.WriteLine(nombre);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            foreach (XmlElement element in xmlDoc.DocumentElement)
            {
                if (element.Name.Equals("appSettings"))
                {

                    foreach (XmlNode node in element.ChildNodes)
                    {
                        //   Debug.WriteLine(node.Attributes[0].Value);
                        if (node.Attributes[0].Value == "nombre")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = nombre;
                        }

                        if (node.Attributes[0].Value == "apellido")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = apellido;
                        }

                        if (node.Attributes[0].Value == "usuario_id")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = id;
                        }
                        if (node.Attributes[0].Value == "usuario")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = usuario;
                        }
                        if (node.Attributes[0].Value == "perfil")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = perfil;
                        }
                    }
                }
            }
            xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            ConfigurationManager.RefreshSection("appSettings");
        }

        public void cargardatosimpresora(String impresora)
        {
            Debug.WriteLine(impresora);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            foreach (XmlElement element in xmlDoc.DocumentElement)
            {
                if (element.Name.Equals("appSettings"))
                {

                    foreach (XmlNode node in element.ChildNodes)
                    {
                        //   Debug.WriteLine(node.Attributes[0].Value);
                        if (node.Attributes[0].Value == "impresora")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = impresora;
                        }


                    }
                }
                xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                ConfigurationManager.RefreshSection("appSettings");
            }
        }


    }
}
