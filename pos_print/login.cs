﻿using System;
using System.Drawing.Printing;
using System.Windows.Forms;
using ESC_POS_USB_NET.Printer;
using WebSocketSharp;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
using pos_print.clase;
using pos_print.conexion;
using System.Configuration;
using System.Diagnostics;
using System.Xml;
namespace pos_print
{
    public partial class formulario_inicia : Form
    {
        public formulario_inicia()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Console.WriteLine("iniciando");
            //titulo.TextAlign = HorizontalAlignment.Center;
            // titulo.Text = "HOLA A TODOS";
            //  lista_impresora.Items.Add("hola prueba");

      
          

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {




        }

        private async void button1_Click_1(object sender, EventArgs e) 
        {
            Login login = new Login();
            login.usuario = input_usuario.Text;
            login.clave = input_clave.Text;
            Console.WriteLine(login);
            string json = JsonConvert.SerializeObject(login);
            Debug.WriteLine(json);
            Conexion conect = new Conexion();
            button1.Enabled = false;
            string result = await conect.Envio_seguro("Web_service/LoginUsuario", json);
           Debug.WriteLine(result);
            // var resp = JsonConvert.DeserializeObject(result);
            LoginResponse resp = JsonConvert.DeserializeObject<LoginResponse>(result);
            button1.Enabled = true;

            if (resp.ok)
            {

                this.cargardatos(resp.nombre,resp.ruc, resp.apellido,resp.empleado_id,resp.perfil_descripcion,resp.usuario);

                principal impre = new principal();
                impre.Show();
                this.Hide();



            }
            else {
                MessageBox.Show(resp.msg,"ERROR");
            }
        }

        public void cargardatos(String nombre,string ruc,String apellido,String id,String perfil,String usuario)
        {
            Debug.WriteLine(nombre);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            foreach (XmlElement element in xmlDoc.DocumentElement) {
                if (element.Name.Equals("appSettings")) {
                  
                    foreach (XmlNode node in element.ChildNodes) {
                     //   Debug.WriteLine(node.Attributes[0].Value);
                        if (node.Attributes[0].Value == "nombre")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = nombre;
                        }

                        if (node.Attributes[0].Value == "apellido")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = apellido;
                        }

                        if (node.Attributes[0].Value == "usuario_id")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = id;
                        }
                        if (node.Attributes[0].Value == "usuario")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = usuario;
                        }
                        if (node.Attributes[0].Value == "perfil")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = perfil;
                        }
                        if (node.Attributes[0].Value == "ruc")
                        {
                            Debug.WriteLine(node.Attributes[0].Value);
                            node.Attributes[1].Value = ruc;
                        }
                    }
                }
            }
            xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            configuracion conficua = new configuracion();
            conficua.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
