﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;


using pos_print.conexion;
using System.Diagnostics;
using Newtonsoft.Json;
using pos_print.clase;
using Quobject.SocketIoClientDotNet.Client;
using Spire.Pdf;
using System.Net;
namespace pos_print
{
    public partial class principal : Form
    {





        public  void iniciar_socket()
        {
            Cliente cliente = new Cliente();
            cliente.idpedido = "2";
            cliente.id_usuario = "1";

            Impresora impre = new Impresora();
            impre.ruc= ConfigurationManager.AppSettings["ruc"];
            impre.nombre_impresora = ConfigurationManager.AppSettings["impresora"];
            impre.usuario = ConfigurationManager.AppSettings["usuario"];


            var socket = IO.Socket("http://localhost:8080");
            string json = JsonConvert.SerializeObject(impre);
            //  string json = JsonConvert.SerializeObject(login);
            object js = JsonConvert.DeserializeObject(json);
            socket.On(Socket.EVENT_CONNECT, () =>
            {
                // socket.Emit("hi");
                //  socket.Emit("iniciar", js);
                socket.Emit("iniciar_impresora", js);
            });

            socket.On("imprimir", (data) =>
            {
                Console.WriteLine(data);
                string impres = ConfigurationManager.AppSettings["impresora"];
               
                String url = "http://facturacion.selvafood.com/downloads/document/pdf/8d282bdc-3e4e-4927-9e60-3e1dcf39737f";

                imprimir(url, impres);

                //socket.Disconnect();
            });

            // foreach (String printer in PrinterSettings.InstalledPrinters)
            //{
            // Console.WriteLine(printer.ToString() + Environment.NewLine);

            //  lista_impresora.Items.Add(printer.ToString());
            // }
        }

        public async void imprimir(string url,string impres)
        {
            using (WebClient client = new WebClient())
            {
                PdfDocument doc = new PdfDocument();
                byte[] bytes = await client.DownloadDataTaskAsync(url);
            doc.LoadFromBytes(bytes);
            doc.PrintSettings.PrinterName = impres;
            doc.Print();
            }


        }

        public principal()
        {
            InitializeComponent();
            iniciar_socket();
            string nombre = ConfigurationManager.AppSettings["nombre"];
            string perfil = ConfigurationManager.AppSettings["perfil"];
            string apellido = ConfigurationManager.AppSettings["apellido"];

            nombre_impresora.Text = ConfigurationManager.AppSettings["impresora"];

            titulo_nombre.Text = nombre.ToUpper() + " "+ apellido.ToUpper();
            titulo_perfil.Text = perfil.ToUpper();
        }
        public async void cargar_informacion()
        {

            String json = "";
            Conexion conect = new Conexion();
          //  button1.Enabled = false;
            string result = await conect.Envio_seguro("Web_service/cargardatosempresa", json);
            Debug.WriteLine(result);
            Empresa resp = JsonConvert.DeserializeObject<Empresa>(result);
            image_logo.WaitOnLoad = false;
            image_logo.LoadAsync(resp.empresa_icono);
            titulo_empresa.Text = resp.empresa_nombre_comercial;



        }

        private void principal_Load(object sender, EventArgs e)
        {
           // image_logo.WaitOnLoad = false;

            //image_logo.LoadAsync(@"https://elpobrecito.com/wp-content/uploads/2021/01/logoweb.png");
            cargar_informacion();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            impresora imp = new impresora();
            imp.ShowDialog();

            if (imp._impresora!="") { 
            
            nombre_impresora.Text=imp._impresora;

            }
            //this.Hide();


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            configuracion config = new configuracion();
            config.ShowDialog();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
